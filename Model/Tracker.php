<?php


namespace MageTemp\ElasticTracker\Model;

use MageTemp\ElasticTracker\Api\Data\TrackerInterface;

class Tracker extends \Magento\Framework\Model\AbstractModel implements TrackerInterface
{

    protected $_eventPrefix = 'magetemp_elastictracker_tracker';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MageTemp\ElasticTracker\Model\ResourceModel\Tracker::class);
    }

    /**
     * Get tracker_id
     * @return string
     */
    public function getTrackerId()
    {
        return $this->getData(self::TRACKER_ID);
    }

    /**
     * Set tracker_id
     * @param string $trackerId
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     */
    public function setTrackerId($trackerId)
    {
        return $this->setData(self::TRACKER_ID, $trackerId);
    }

    /**
     * Get id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
}

<?php


namespace MageTemp\ElasticTracker\Model\ResourceModel;

class Tracker extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magetemp_elastictracker_tracker', 'tracker_id');
    }
}

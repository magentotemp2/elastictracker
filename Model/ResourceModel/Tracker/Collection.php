<?php


namespace MageTemp\ElasticTracker\Model\ResourceModel\Tracker;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MageTemp\ElasticTracker\Model\Tracker::class,
            \MageTemp\ElasticTracker\Model\ResourceModel\Tracker::class
        );
    }
}

<?php


namespace MageTemp\ElasticTracker\Model;

use MageTemp\ElasticTracker\Api\TrackerRepositoryInterface;
use MageTemp\ElasticTracker\Api\Data\TrackerSearchResultsInterfaceFactory;
use MageTemp\ElasticTracker\Api\Data\TrackerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use MageTemp\ElasticTracker\Model\ResourceModel\Tracker as ResourceTracker;
use MageTemp\ElasticTracker\Model\ResourceModel\Tracker\CollectionFactory as TrackerCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class TrackerRepository implements TrackerRepositoryInterface
{

    protected $resource;

    protected $trackerFactory;

    protected $trackerCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataTrackerFactory;

    private $storeManager;

    /**
     * @param ResourceTracker $resource
     * @param TrackerFactory $trackerFactory
     * @param TrackerInterfaceFactory $dataTrackerFactory
     * @param TrackerCollectionFactory $trackerCollectionFactory
     * @param TrackerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceTracker $resource,
        TrackerFactory $trackerFactory,
        TrackerInterfaceFactory $dataTrackerFactory,
        TrackerCollectionFactory $trackerCollectionFactory,
        TrackerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->trackerFactory = $trackerFactory;
        $this->trackerCollectionFactory = $trackerCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataTrackerFactory = $dataTrackerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
    ) {
        /* if (empty($tracker->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $tracker->setStoreId($storeId);
        } */
        try {
            $this->resource->save($tracker);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the tracker: %1',
                $exception->getMessage()
            ));
        }
        return $tracker;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($trackerId)
    {
        $tracker = $this->trackerFactory->create();
        $this->resource->load($tracker, $trackerId);
        if (!$tracker->getId()) {
            throw new NoSuchEntityException(__('Tracker with id "%1" does not exist.', $trackerId));
        }
        return $tracker;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->trackerCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
    ) {
        try {
            $this->resource->delete($tracker);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Tracker: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($trackerId)
    {
        return $this->delete($this->getById($trackerId));
    }
}

<?php


namespace MageTemp\ElasticTracker\Api\Data;

interface TrackerInterface
{

    const TRACKER_ID = 'tracker_id';
    const ID = 'id';

    /**
     * Get tracker_id
     * @return string|null
     */
    public function getTrackerId();

    /**
     * Set tracker_id
     * @param string $trackerId
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     */
    public function setTrackerId($trackerId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     */
    public function setId($id);
}

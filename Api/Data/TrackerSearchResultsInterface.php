<?php


namespace MageTemp\ElasticTracker\Api\Data;

interface TrackerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Tracker list.
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \MageTemp\ElasticTracker\Api\Data\TrackerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

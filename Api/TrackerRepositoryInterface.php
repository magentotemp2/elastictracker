<?php


namespace MageTemp\ElasticTracker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface TrackerRepositoryInterface
{

    /**
     * Save Tracker
     * @param \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
    );

    /**
     * Retrieve Tracker
     * @param string $trackerId
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($trackerId);

    /**
     * Retrieve Tracker matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MageTemp\ElasticTracker\Api\Data\TrackerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Tracker
     * @param \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \MageTemp\ElasticTracker\Api\Data\TrackerInterface $tracker
    );

    /**
     * Delete Tracker by ID
     * @param string $trackerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($trackerId);
}

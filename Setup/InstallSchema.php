<?php


namespace MageTemp\ElasticTracker\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_magetemp_elastictracker_tracker = $setup->getConnection()->newTable($setup->getTable('magetemp_elastictracker_tracker'));

        $table_magetemp_elastictracker_tracker->addColumn(
            'tracker_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_magetemp_elastictracker_tracker->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'id'
        );

        $setup->getConnection()->createTable($table_magetemp_elastictracker_tracker);
    }
}
